package ru.enqueue.factory.controller;

import lombok.RequiredArgsConstructor;
import org.hibernate.SimpleNaturalIdLoadAccess;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.enqueue.factory.entity.User;
import ru.enqueue.factory.entity.UserQueueInfo;
import ru.enqueue.factory.service.api.ManagerQueueService;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class ManagerQueueController {

    private final ManagerQueueService managerQueueService;

    @RequestMapping(value = "/manager/{queueName}", method = RequestMethod.GET)
    public ResponseEntity<List<User>> getListOfUsers(
            @PathVariable("queueName") String queueName
    ) {
        List<User> allUsersInQueue = managerQueueService.getAllUsersInQueue(queueName);
        return new ResponseEntity<>(allUsersInQueue, HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/{queueName}/next", method = RequestMethod.POST)
    public ResponseEntity<User> setNextUser(
            @PathVariable("queueName") String queueName) {
        User user = managerQueueService.setNext(queueName);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/manager/{queueName}/current", method = RequestMethod.GET)
    public ResponseEntity<User> getCurrentUserInfo(
            @PathVariable("queueName") String queueName) {
        User currentUserInfo = managerQueueService.getCurrentUserInfo(queueName);
        return new ResponseEntity<>(currentUserInfo, HttpStatus.OK);
    }



}
;