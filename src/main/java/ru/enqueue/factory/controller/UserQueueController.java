package ru.enqueue.factory.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.enqueue.factory.entity.UserQueueInfo;
import ru.enqueue.factory.service.api.UserQueueService;

@RestController
@RequiredArgsConstructor
public class UserQueueController {
    public final UserQueueService userQueueService;

    @RequestMapping(value = "/client/{queueName}", method = RequestMethod.POST)
    public ResponseEntity<UserQueueInfo> enterTheQueue(
            @PathVariable("queueName") String queueName,
            @RequestParam String deviceId,
            @RequestParam String username
            ) {
        UserQueueInfo userQueueInfo = userQueueService.enterToQueue(queueName, deviceId, username);
        return new ResponseEntity<>(userQueueInfo, HttpStatus.OK);
    }

    @RequestMapping(value = "/client", method = RequestMethod.GET)
    public ResponseEntity<UserQueueInfo> getUserQueueInfo(
            @RequestParam String deviceId) {
        UserQueueInfo userQueueInfo = userQueueService.getUserQueueInfo(deviceId);
        return new ResponseEntity<UserQueueInfo>(userQueueInfo, HttpStatus.OK);
    }

    @RequestMapping(value = "/client", method = RequestMethod.DELETE)
    public ResponseEntity<UserQueueInfo> leaveTheQueue(
            @RequestParam String deviceId) {
        UserQueueInfo userQueueInfo = userQueueService.leaveQueue(deviceId);
        return new ResponseEntity<>(userQueueInfo, HttpStatus.OK);
    }
}
