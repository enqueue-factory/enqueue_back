package ru.enqueue.factory.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.enqueue.factory.entity.Queue;
import ru.enqueue.factory.service.api.QueueService;

@RestController
@RequiredArgsConstructor
public class QueueController {

    public final QueueService queueService;

    @RequestMapping(value = "/manage/createQueue", method = RequestMethod.POST)
    public ResponseEntity<Queue> createQueue(
            @RequestParam String queueName){
        Queue queue = queueService.createQueue(queueName);
        return new ResponseEntity<>(queue, HttpStatus.OK);
    }

    @RequestMapping(value = "/manage/deleteQueue", method = RequestMethod.DELETE)
    public ResponseEntity<Queue> deleteQueue(
            @RequestParam String queueName){
        return new ResponseEntity<>(queueService.deleteQueue(queueName), HttpStatus.OK);
    }

    @RequestMapping(value ="manage/isExist/{queueName}", method = RequestMethod.GET)
    public ResponseEntity<Boolean> isExist(
            @PathVariable("queueName") String queueName){
        return new ResponseEntity<>(queueService.isExist(queueName), HttpStatus.OK);
    }

}
