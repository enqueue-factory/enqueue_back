package ru.enqueue.factory.entity;

import lombok.*;
import javax.persistence.*;

@Entity
@Table(name = "Queue")
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Queue {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name", unique = true)
    private String name;
    @Column(name = "isActive")
    private boolean isActive;
}
