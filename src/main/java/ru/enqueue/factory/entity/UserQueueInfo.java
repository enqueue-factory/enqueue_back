package ru.enqueue.factory.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserQueueInfo {
    private User user;
    private Queue queue;
    private Long place;
}
