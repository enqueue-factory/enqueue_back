package ru.enqueue.factory.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import ru.enqueue.factory.entity.Queue;
import ru.enqueue.factory.repository.QueueRepository;
import ru.enqueue.factory.repository.UserRepository;
import ru.enqueue.factory.service.api.QueueService;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class QueueServiceImpl implements QueueService {

    private final QueueRepository queueRepository;
    private final UserRepository userRepository;
    private final RedisTemplate<String, String> redisTemplate;
    private ListOperations<String, String> listOperations;

    @PostConstruct
    public void init() {
        listOperations = redisTemplate.opsForList();
    }

    @Override
    public Queue createQueue(String queueName) {
        if (queueRepository.existsByName(queueName)) {
            throw new IllegalArgumentException("Queue already exists");
        }
        Queue queue = Queue.builder()
                .name(queueName)
                .isActive(true)
                .build();
        return queueRepository.save(queue);
    }

    @Override
    public Queue getQueue(String queueName) {
        Optional<Queue> queueOptional = queueRepository.findByName(queueName);
        if (queueOptional.isPresent()) {
            return queueOptional.get();
        } else throw new IllegalArgumentException("");
    }

    @Override
    @Transactional
    public Queue deactivateQueue(String queueName) {
        Optional<Queue> queueOptional = queueRepository.findByName(queueName);
        if (queueOptional.isPresent()) {
            Queue queue = queueOptional.get();
            queue.setActive(false);
            return queue;
        } else throw new IllegalArgumentException("");
    }

    @Override
    @Transactional
    public Queue activateQueue(String queueName) {
        Optional<Queue> queueOptional = queueRepository.findByName(queueName);
        if (queueOptional.isPresent()) {
            Queue queue = queueOptional.get();
            queue.setActive(true);
            return queue;
        } else throw new IllegalArgumentException("");
    }

    @Override
    @Transactional
    public Queue deleteQueue(String queueName) {
        Optional<Queue> queueOptional = queueRepository.findByName(queueName);
        if (queueOptional.isPresent()) {
            Queue queue = queueOptional.get();
            userRepository.deleteAllByQueue(queue);
            queueRepository.delete(queue);
            listOperations.trim(queue.getName(), 0, Long.MAX_VALUE);
            return queue;
        } else throw new IllegalArgumentException("");
    }

    @Override
    public Queue isActive(String queueName) {
        Optional<Queue> queueOptional = queueRepository.findByName(queueName);
        if (queueOptional.isPresent()) {
            Queue queue = queueOptional.get();
            queueRepository.delete(queue);
            return queue;
        } else throw new IllegalArgumentException("");
    }

    @Override
    public Boolean isExist(String queueName) {
        return queueRepository.existsByName(queueName);
    }

}
