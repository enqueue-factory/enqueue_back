package ru.enqueue.factory.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import ru.enqueue.factory.entity.Queue;
import ru.enqueue.factory.entity.User;
import ru.enqueue.factory.entity.UserQueueInfo;
import ru.enqueue.factory.repository.UserRepository;
import ru.enqueue.factory.service.api.QueueService;
import ru.enqueue.factory.service.api.UserQueueService;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class UserQueueServiceImpl implements UserQueueService {

    private final UserRepository userRepository;
    private final QueueService queueService;
    private final RedisTemplate<String, String> redisTemplate;
    private ListOperations<String, String> listOperations;

    @PostConstruct
    public void init() {
        listOperations = redisTemplate.opsForList();
    }

    @Override
    @Transactional
    public UserQueueInfo enterToQueue(String queueName, String deviceId, String userName) {
        Queue queue = queueService.getQueue(queueName);
        User user = User.builder()
                .name(userName)
                .deviceId(deviceId)
                .identifier(UUID.randomUUID().toString().substring(0, 6))
                .queue(queue)
                .build();
        listOperations.rightPush(queue.getName(), user.getDeviceId());
        userRepository.save(user);
        return UserQueueInfo.builder()
                .user(user)
                .queue(queue)
                .place(listOperations.indexOf(queue.getName(), user.getDeviceId()))
                .build();
    }

    @Override
    @Transactional
    public UserQueueInfo getUserQueueInfo(String deviceId) {
        Optional<User> userOptional = userRepository.findByDeviceId(deviceId);
        if (!userOptional.isPresent()) {
            throw new IllegalArgumentException("User doesnt exist");
        }
        Long place = listOperations.indexOf(userOptional.get().getQueue().getName(), deviceId);
        return UserQueueInfo.builder()
                .user(userOptional.get())
                .queue(userOptional.get().getQueue())
                .place(place)
                .build();
    }

    @Override
    @Transactional
    public UserQueueInfo leaveQueue(String deviceId) {
        Optional<User> userOptional = userRepository.findByDeviceId(deviceId);
        if(!userOptional.isPresent()) {
            throw new IllegalArgumentException("User doesnt exist");
        }
        listOperations.remove(userOptional.get().getQueue().getName(), 1, deviceId);
        userRepository.delete(userOptional.get());
        return UserQueueInfo.builder()
                .user(userOptional.get())
                .queue(userOptional.get().getQueue())
                .place(-1L)
                .build();
    }
}
