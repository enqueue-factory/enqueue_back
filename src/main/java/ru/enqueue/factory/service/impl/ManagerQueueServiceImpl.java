package ru.enqueue.factory.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.ListOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import ru.enqueue.factory.entity.Queue;
import ru.enqueue.factory.entity.User;
import ru.enqueue.factory.repository.UserRepository;
import ru.enqueue.factory.service.api.ManagerQueueService;
import ru.enqueue.factory.service.api.QueueService;
import ru.enqueue.factory.service.api.UserQueueService;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ManagerQueueServiceImpl implements ManagerQueueService {

    private final UserRepository userRepository;
    private final RedisTemplate<String, String> redisTemplate;
    private ListOperations<String, String> listOperations;

    @PostConstruct
    public void init() {
        listOperations = redisTemplate.opsForList();
    }

    @Override
    public List<User> getAllUsersInQueue(String queueName) {
        return userRepository.findAllByDeviceIdIn(listOperations.range(queueName, 0, Long.MAX_VALUE));
    }

    @Override
    @Transactional
    public User setNext(String queueName) {
        String currentUserDeviceId = listOperations.leftPop(queueName);
        if (currentUserDeviceId != null) {
            userRepository.deleteByDeviceId(currentUserDeviceId);
        }
        String deviceId = listOperations.range(queueName, 0, 0).get(0);
        return userRepository.findByDeviceId(deviceId).get();
    }

    @Override
    public User getCurrentUserInfo(String queueName) {
        String deviceId = listOperations.range(queueName, 0, 0).get(0);
        return userRepository.findByDeviceId(deviceId).get();
    }
}
