package ru.enqueue.factory.service.api;

import ru.enqueue.factory.entity.Queue;

public interface QueueService {
    Queue createQueue(String queueName);
    Queue getQueue(String queueName);
    Queue deactivateQueue(String queueName);
    Queue activateQueue(String queueName);
    Queue deleteQueue(String queueName);
    Queue isActive(String queueName);
    Boolean isExist(String queueName);
}
