package ru.enqueue.factory.service.api;

import ru.enqueue.factory.entity.UserQueueInfo;

public interface UserQueueService {
    UserQueueInfo enterToQueue(String queueName,
                               String deviceId,
                               String userName);

    UserQueueInfo getUserQueueInfo(String deviceId);

    UserQueueInfo leaveQueue(String deviceId);


}
