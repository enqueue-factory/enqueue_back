package ru.enqueue.factory.service.api;

import ru.enqueue.factory.entity.User;

import java.util.List;

public interface ManagerQueueService {
    List<User> getAllUsersInQueue(String queueName);
    User setNext(String queueName);
    User getCurrentUserInfo(String queueName);
}
