package ru.enqueue.factory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.enqueue.factory.entity.Queue;

import java.util.Optional;

@Repository
public interface QueueRepository extends JpaRepository<Queue, Long> {
    boolean existsByName(String name);
    Optional<Queue> findByName(String name);
}
