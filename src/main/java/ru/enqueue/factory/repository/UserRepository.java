package ru.enqueue.factory.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.enqueue.factory.entity.Queue;
import ru.enqueue.factory.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByDeviceId(String deviceId);
    List<User> findAllByDeviceIdIn(List<String> deviceId);
    void deleteAllByQueue(Queue name);
    void deleteByDeviceId(String deviceId);
}
